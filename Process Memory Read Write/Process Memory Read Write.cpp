﻿#include "stdafx.h" 
#include <windows.h> 
#include <TlHelp32.h> 
#include <iostream>
#include <thread>
#include <chrono>
#define F11 0x7A
using namespace std ;

DWORD getmodulebase(DWORD dwProcessIdentifier, TCHAR *lpszModuleName){
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwProcessIdentifier) ;
	DWORD dwModuleBaseAddress = 0 ;
	if (hSnapshot != INVALID_HANDLE_VALUE) {
		MODULEENTRY32 ModuleEntry32 = { 0 } ;
		ModuleEntry32.dwSize = sizeof(MODULEENTRY32) ;
		if (Module32First(hSnapshot, &ModuleEntry32)){
			do {
				if (_tcscmp(ModuleEntry32.szModule, lpszModuleName) == 0) {
					dwModuleBaseAddress = (DWORD)ModuleEntry32.modBaseAddr ;
					break ;
				}
			} while (Module32Next(hSnapshot, &ModuleEntry32)) ;
		}
		CloseHandle(hSnapshot) ;
	}
	return dwModuleBaseAddress ;
}

int main()
{
	HWND window = FindWindow(0, _T("Resident Evil 0 / biohazard 0　HD REMASTER")) ;
	DWORD pid, value, valuetwo ;
	int increment = 15 ;
	if (!window) {
		printf("Unable to find window") ;
		cin.get() ;
		return 0 ;
	} else {
		GetWindowThreadProcessId(window, &pid) ;
		cout << "proccess: " << pid << " pid." << endl ;
		DWORD baseAddr = getmodulebase(pid, _T("re0hd.exe")) ;
		DWORD staticone = 0x009D1084 ;
		HANDLE handle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid) ;
		cout << "Proccess Opened: .. " << endl ;
		while (!GetAsyncKeyState(0x7A)) {
			this_thread::sleep_for(2s) ;
			DWORD read ;
			ReadProcessMemory(handle, (LPCVOID)(baseAddr + staticone), &value, sizeof(DWORD), &read) ;
			value += 0xb4 ;
			ReadProcessMemory(handle, (LPCVOID)value, &value, sizeof(DWORD), &read) ;
			valuetwo = value ;
			value += 0x58 ;
			ReadProcessMemory(handle, (LPCVOID)value, &value, sizeof(DWORD), &read) ;
			valuetwo += 0x58 ;
			/* cout << "Value: " << (LPCVOID)valuetwo ;
			   cout << "Value: " << value ; */
			WriteProcessMemory(handle, (void *)(LPCVOID)valuetwo, &increment, (DWORD)sizeof(increment), NULL) ;
		}
		cin.get() ;
	}
    return 0 ;
}